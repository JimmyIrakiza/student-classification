package controller;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import common.DbConstant;
import common.JSFBoundleProvider;
import common.JSFMessagers;
import common.SessionUtils;
import dao.impl.LoginHistoricImpl;
import dao.impl.LoginImpl;
import dao.impl.UserImpl;
import domain.LoginHistoric;
import domain.User;




@ManagedBean
@SessionScoped
public class LoginController implements Serializable, DbConstant {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	private String CLASSNAME = "LoginController :: ";
	private static final long serialVersionUID = 1L;
	/* to manage validation messages */
	private boolean isValid;
	/* end manage validation messages */
	private User user;
	/* begin class injection */
	JSFBoundleProvider provider = new JSFBoundleProvider();
	LoginImpl loginDao1 = new LoginImpl();
	UserImpl usersImpl = new UserImpl();
	LoginHistoricImpl historic = new LoginHistoricImpl();
	/* end class injection */
	boolean isValidCredential;
	Timestamp timestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
	
	@PostConstruct
	public void init() {
		if (user == null) {
			user = new User();
		}

	}
	
	public String validateUsernamePassword() {
		LOGGER.info(CLASSNAME + ":::step one");
		HttpSession session = SessionUtils.getSession();
		if(session.getAttribute("userSession")!=null) {
			session.invalidate();	
		}
		
		try {
			user = usersImpl.getModelWithMyHQL(new String[] { USERNAME, PASSWORD },
					new Object[] { user.getViewId(), loginDao1.criptPassword(user.getViewName()) }, SELECT_USERS);
			LOGGER.info("check username and password done :::" + user.getViewId());
			if (user != null) {
				isValidCredential = true;
				LOGGER.info("check username and password is correct:::");
			}else {
				isValidCredential = false;
				System.out.println("Changing Credintials 7");
				setValid(false);
				JSFMessagers.addErrorMessage(getProvider().getValue("com.validat.user.password"));
				LOGGER.info("check username and password is incorrect:::");
			}
			
			LOGGER.info("::: USer Credi: "+ user.getViewId() + " And "+ user.getViewName());
			
			if (user != null && (user.getStatus()).equals(ACTIVE)) {
			} else {
				isValidCredential = false;
			}
		} catch (Exception jj) {
			LOGGER.info("errrr::::::::: "+ jj.getCause()+ " ANd "+ jj.getMessage());
			setValid(false);
			JSFMessagers.addErrorMessage(getProvider().getValue("com.server.side.internal.error"));
			LOGGER.info(jj.getMessage());
			jj.printStackTrace();
			setValid(false);
			JSFMessagers.addErrorMessage(getProvider().getValue("com.server.side.internal.error"));
		}
		
		// Check if the account is locked
		
		System.out.println("Validity::: "+ isValidCredential);
		
		if(isValidCredential) {
			try {
				LOGGER.info(CLASSNAME + "LoginHistoric saving start for machine ip" + historic.getMachineIp());

				//LoginHistoric his = new LoginHistoric();
//				his.setHistoricId(0);
//				his.setIpAddress(historic.getMachineIp());
//				his.setLoginTimeIn(new Date());
//				his.setCreatedBy(user.getFname() + " " + user.getLname());
//				his.setUpDtTime(timestamp);
//
//				his.setUsers(user);
				session.setAttribute("userSession", user);

//				Object a = historic.saveLoginHistoric(his);
				// session.setAttribute("loginID", a);
				LOGGER.info(CLASSNAME + "Loging Save Login Historic");
				LOGGER.info("step 111");
				user.setLoginStatus(ONLINE);
				usersImpl.UpdateUsers(user);

				LOGGER.info(CLASSNAME + "Creating Sessions for users::" + user.getViewId());
				
			} catch (Exception ex) {
				LOGGER.info(CLASSNAME + "Loging Fail when login" + ex.getMessage());
				setValid(false);
				JSFMessagers.addInfoMessage("com.server.side.internal.error");
				ex.printStackTrace();
			}
			
			System.out.println("::::USER INFO: "+ user.getUserCategory().getUsercategoryName());
			LOGGER.info("::::USER INFO: "+ user.getUserCategory());
			//loggedInUser = loginDao1.userDetail(user.getViewId());
			if (user != null && user.getUserCategory().getUserCatid() == 1) {// ADMIN

				LOGGER.info(CLASSNAME + ":::ADMIN ");

				return "/templates/hec/hecDashboard.xhtml?faces-redirect=true";
				// return "";
			} else {

				LOGGER.info("login on 1 page for all users ");
				return "/public/registrationPage.xhtml?faces-redirect=true";
			}
		}else {
			LOGGER.info(CLASSNAME + "credential are invalid ");
			JSFMessagers.resetMessages();
			setValid(false);
			JSFMessagers.addErrorMessage(getProvider().getValue("com.validat.user.password"));
			return "";	
		}
	}

	public void logout() {
		HttpSession session = SessionUtils.getSession();

		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();

		String url = request.getContextPath() + "/home.xhtml";
		try {
			
			User user= new User();
			user=(User)session.getAttribute("userSession");
			LoginHistoric his = new LoginHistoric();

			his.setHistoricId(0);
			his.setIpAddress(historic.getMachineIp());
			his.setLogOutTime(new Date());
			his.setCreatedBy(user.getFname() + " " + user.getLname());
			his.setUpDtTime(timestamp);
			his.setUsers(user);
			historic.saveLoginHistoric(his);
			user.setLoginStatus(OFFLINE);
			usersImpl.UpdateUsers(user);
			FacesContext.getCurrentInstance().getExternalContext().redirect(url);
		} catch (IOException e) {

			e.printStackTrace();
		}
		session.invalidate();
	}
	
	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public JSFBoundleProvider getProvider() {
		return provider;
	}

	public void setProvider(JSFBoundleProvider provider) {
		this.provider = provider;
	}

	public LoginImpl getLoginDao1() {
		return loginDao1;
	}

	public void setLoginDao1(LoginImpl loginDao1) {
		this.loginDao1 = loginDao1;
	}

	public UserImpl getUsersImpl() {
		return usersImpl;
	}

	public void setUsersImpl(UserImpl usersImpl) {
		this.usersImpl = usersImpl;
	}

	public static Logger getLogger() {
		return LOGGER;
	}
	
	
	
}
