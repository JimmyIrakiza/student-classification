package controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import common.JSFBoundleProvider;
import dao.impl.LoginHistoricImpl;
import dao.impl.LoginImpl;
import dao.impl.UserImpl;
import domain.User;


@ManagedBean(name = "regController")
public class RegistrationController {
	User user = new User();
	

	public String message = "Registration Page";
	
	/* begin class injection */
	JSFBoundleProvider provider = new JSFBoundleProvider();
	LoginImpl loginDao1 = new LoginImpl();
	UserImpl usersImpl = new UserImpl();
	LoginHistoricImpl historic = new LoginHistoricImpl();
	
	public List<User> users;
	
	@PostConstruct
	public void init() {
		if (users == null) {
			users = usersImpl.getListUsers();
		}

	}
	
	public String register() {
		try {
			LoginImpl loginImpl = new LoginImpl();
			user.setStatus("active");
			user.setViewName(loginImpl.criptPassword(user.getViewId()));
			new UserImpl().saveIntable(user);
			return "/public/login.xhtml";
		} catch (Exception e) {
			return "/public/registrationPage.xhtml";
		}
	}
	
		
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}





	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}





	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
