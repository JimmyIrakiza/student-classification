package controller;

import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import dao.impl.UserImpl;

@ManagedBean
@SessionScoped
public class BaseController {

	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());

	public void createAllTable() {
		UserImpl userImpl = new UserImpl();
		userImpl.creatingNewTable();
		LOGGER.info("table created");
	}
	
	public String getContextPath() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		return request.getContextPath();
	}
		
	
}
