package dao.interfaces;

import java.util.List;

import domain.LoginHistoric;




public interface ILoginHistoric {
	public LoginHistoric saveLoginHistoric(LoginHistoric loginHistoric);

	public List<LoginHistoric> getListLoginHistoric();

	public LoginHistoric getLoginHistoricById(int loginHistoricId, String primaryKeyclomunName);

	public LoginHistoric UpdateLoginHistoric(LoginHistoric loginHistoric);

	public String getMachineIp();
}
