package dao.impl;

import java.util.List;

import dao.generic.AbstractDao;
import dao.interfaces.IUserCategory;
import domain.UserCategory;


public class UserCategoryImpl extends AbstractDao<Long, UserCategory> implements IUserCategory {

	public UserCategory saveUsercategory(UserCategory usercategory) {

		return saveIntable(usercategory);
	}

	public List<UserCategory> getListUsercategory() {

		return (List<UserCategory>) (Object) getModelList();
	}

	public UserCategory getUserCategoryById(int usercatId, String primaryKeyColumn) {
		return (UserCategory) getModelById(usercatId, primaryKeyColumn);

	}

	public UserCategory UpdateUsercategory(UserCategory usercategory) {

		return updateIntable(usercategory);
	}

	

}
